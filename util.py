from glob import glob
import numpy as np
import os
import pandas as pd
import re
from zipfile import ZipFile



def read_timeseries(filename):
    return pd.read_csv(filename, comment='#', index_col='ts', parse_dates=True)


"""
Called in read_multi_zip_from_dir through read_multi_csv_from_zip
"""

def sorted_csv_files(file_list):
    files = []
    for f in file_list:
        if re.match('.*\d{6}.csv', f):
            files.append([-1, f])
        else:
            m = re.match('.*\((\d+)\).csv', f)
            if m:
                files.append([int(m.groups()[0]), f])
    files = sorted(files)
    return [f[1] for f in files]


"""
Hx, Hy and Hz need to be the first 3 columns of DataFrame
"""
def derivative(file_to_derivative, inplace=False):
    if inplace:
        f_t_d = file_to_derivative
    else:
        f_t_d = file_to_derivative.copy()
    f_t_d.iloc[:,0:3] = (f_t_d.iloc[:,0:3].shift(-1) - f_t_d.iloc[:,0:3].shift(1)) / 2
    # if len(f_t_d.columns) > 3:
    # f_t_d[['Hx.nT','Hy.nT','Hz.nT']]=(f_t_d[['Hx.nT','Hy.nT','Hz.nT']])
    # else:

    return f_t_d

"""
One columns derivative
"""
def one_val_der(file_derivative, inplace=False):
    if inflace:
        ftd=file_derivative
    else:
        ftd=file_derivative.copy()
    ftd=(ftd.shift(-1)-ftd.shift(1))/2

    return ftd

"""
Called from read_multi_zip_from_dir
"""

def read_multi_csv_from_zip(filename, trace=False, res='S'):
    file_date = pd.Timestamp(os.path.split(filename)[-1][:8])
    x = []
    zf = ZipFile(filename)
    for f in sorted_csv_files(zf.namelist()):
        lines = zf.open(f).readlines()
        if len(lines) > 0:
            if trace:
                print(f'reading {f} ...')
            lines = [line.decode('unicode_escape') for line in lines]
            lines = [[token.replace(',', '.').strip() for token in line.split(';')[:8]] for line in lines]
            lines = [line for line in lines if len(line) == 8]
            for line in lines[1:]:
                if len(line[0]) != 8:
                    line[0] = 'NaT' #np.isnat #'NaT'
                for i in range(1, len(line)):
                    try:
                        line[i] = float(line[i])
                    except ValueError:
                        line[i] = np.nan
            data = pd.DataFrame(lines[1:], columns=lines[0]).set_index('UTC time')
            if len(data) > 0:
                data.index = pd.to_datetime(data.index)
                start_time = data.index[0]
                data_today = data[data.index >= start_time]
                data_today.index = data_today.index - data_today.index[0].floor('D') + file_date
                x.append(data_today)
                data_tomorrow = data[data.index < start_time]
                if len(data_tomorrow) > 0:
                    file_date += pd.Timedelta('1D')
                    data_tomorrow.index = data_tomorrow.index - data_tomorrow.index[0].floor('D') + file_date
                    x.append(data_tomorrow)
            else:
                if trace:
                    print(f'zero rows in {f}')
        else:
            if trace:
                print(f'no data in {f}')

    x = pd.concat(x).sort_index().resample(res).mean()

    return x


"""
Use this one to read in data from measurements
"""
def read_multi_zip_from_dir(dirname, trace=False, res='S', pattern='*.zip'):
    x = [read_multi_csv_from_zip(f, trace=trace, res=res) for f in glob(os.path.join(dirname, pattern))]
    for i in range(1, len(x)):
        x[i].index += x[i-1].index[-1].date() - x[i].index[0].date()
    return pd.concat(x)


"""
Function to clean out "extreme" values regarded as errors use this once Hx, Hy, Hz is correctly orientated, i.e. all are positive
This is to clean the total magnetic field
"""
def clean_total(x, inplace=False):
    if inplace:
        y = x
    else:
        y = x.copy()
    y.loc[y['t.°C'] < -50, 't.°C'] = np.nan
    y.loc[y['t.°C'] > 80, 't.°C'] = np.nan
    y.loc[y['Hx.nT'] > 3e4, 'Hx.nT'] = np.nan
    y.loc[y['Hy.nT'] > 2e4, 'Hy.nT'] = np.nan
    y.loc[y['Hz.nT'] > 6e4, 'Hz.nT'] = np.nan
    y.loc[y['Hx.nT'] < 0, 'Hx.nT'] = np.nan
    y.loc[y['Hy.nT'] < -2e4, 'Hy.nT'] = np.nan
    y.loc[y['Hz.nT'] < 0, 'Hz.nT'] = np.nan
    return y


"""
This is to clean values of a derivative df
"""
def clean_derivative(x, inplace=False):
    if inplace:
        y = x
    else:
        y = x.copy()
    y.loc[y['t.°C'] < -50, 't.°C'] = np.nan
    y.loc[y['t.°C'] > 80, 't.°C'] = np.nan
    y.loc[y['Hx.nT'] > 1e3, 'Hx.nT'] = np.nan
    y.loc[y['Hy.nT'] > 1e3, 'Hy.nT'] = np.nan
    y.loc[y['Hz.nT'] > 1e3, 'Hz.nT'] = np.nan
    y.loc[y['Hx.nT'] < -1e3, 'Hx.nT'] = np.nan
    y.loc[y['Hy.nT'] < -1e3, 'Hy.nT'] = np.nan
    y.loc[y['Hz.nT'] < -1e3, 'Hz.nT'] = np.nan
    return y


"""
Clean function for error spikes
"""
def clean_jumps(x, inplace=False):
    if inplace:
        y = x
    else:
        y = x.copy()
    y.loc[y['Hx.nT'].shift(-1)-y['Hx.nT'] > 1e3, 'Hx.nT'] = np.nan
    y.loc[y['Hy.nT'].shift(-1)-y['Hy.nT'] > 1e3, 'Hy.nT'] = np.nan
    y.loc[y['Hz.nT'].shift(-1)-y['Hz.nT'] > 1e3, 'Hz.nT'] = np.nan
    y.loc[y['Hx.nT'].shift(-1)-y['Hx.nT'] < -1e3, 'Hx.nT'] = np.nan
    y.loc[y['Hy.nT'].shift(-1)-y['Hy.nT'] < -1e3, 'Hy.nT'] = np.nan
    y.loc[y['Hz.nT'].shift(-1)-y['Hz.nT'] < -1e3, 'Hz.nT'] = np.nan
    return y




"""
Used for miss-oriented axis to get proper orientation, or warn if anything is wrong
"""
def orientate_mag(g, inplace=False): # if the fgsensor is oriented wrong, this fixes it
    if inplace:
        h=g
    else:
        h=g.copy()
    h['Hx.nT'] = -h['Hx.nT']
    h['Hy.nT'] = h['Hy.nT']
    h['Hz.nT'] = h['Hz.nT']
    if (h['Hz.nT'].mean() > h['Hx.nT'].mean() and h['Hx.nT'].mean() > h['Hy.nT'].mean()):
        print('Orientation seems to be correct')
    else:
        print('Something seems to be off with the orientation')
    return h


"""
To smooth out data from errors, mainly for single error values
"""
def roll_median_file(r, npm): # f data to roll, npm number points median, amount of values to roll median over, ideally 3
    f_rv=r[['Hx.nT','Hy.nT','Hz.nT']]
    f_r=f_rv.rolling(npm, center=True).median()

    return f_r

"""
Functions to read in data from TOP station
"""

def translate(measured):
    """
    Translate from measured (relative) xyz components to true xyz components, where x points towards north along the
    local magnetic field direction.
    :param measured: Nx3 Numpy array with columns in xyz order.
    :return: Nx3 Numpy array with true values.
    """
    x_steps, y_steps = 112, 63
    scale = 150
    return measured + np.array([scale * x_steps, 0, 38000 + scale * y_steps])


def rotate(xyz_local):
    """
    Rotate the true xyz components to geographic components.
    :param xyz_local: Nx3 Numpy array with true local xyz.
    :return: Nx3 Numpy array in geographic XYZ system.
    """
    d0 = 3.89/180*np.pi
    t = np.array([[np.cos(d0), np.sin(d0), 0],
                  [-np.sin(d0), np.cos(d0), 0],
                  [0, 0, 1]])
    m = np.matmul(xyz_local, t)
    if isinstance(m, pd.DataFrame):
        m.columns = xyz_local.columns
    return m


def measured_to_xyz(measured):
    """
    Transform measured relative xyz components to true geographic XYZ components.
    :param measured: Nx3 Numpy array with columns in xyz order.
    :return: Nx3 Numpy array in geographic XYZ system.
    """
    return rotate(translate(measured))


def xyz_to_hdz(xyz):
    """
    Transform XYZ to HDZ.
    :param xyz: Nx3 Numpy array in XYZ.
    :return: Nx3 Numpy array in HDZ.
    """
    x, y, z = xyz.T
    h = np.sqrt(x**2+y**2)
    d = 180/np.pi*np.arctan(y/x)
    return np.column_stack([h, d, z])




"""
Function 1 for when measurements is only one csv file.
"""

def read_csvfile(filename, trace=False, res='S'):
    file_date = pd.Timestamp(os.path.split(filename)[0][15:23])
    x = []
    data = pd.read_csv(filename, sep=';', decimal=',', parse_dates=True, index_col='UTC time',
                       encoding='unicode_escape')
    data = data[data.columns[:-1]].astype(float)
    start_time = data.index[0]
    data_today = data[data.index >= start_time]
    data_today.index = data_today.index - data_today.index[0].floor('D') + file_date
    x.append(data_today)
    data_tomorrow = data[data.index < start_time]
    if len(data_tomorrow) > 0:
        file_date += pd.Timedelta('1D')
        data_tomorrow.index = data_tomorrow.index - data_tomorrow.index[0].floor('D') + file_date
        x.append(data_tomorrow)

    else:
        if trace:
            print(f'no data in {f}')
    x = pd.concat(x).sort_index().resample(res).mean()

    return x


"""
Function 2 for when measurements is only one csv file.
"""
def get_csv_fgsensors(filename, trace=False, res='S'): # path [0] last 8 digit [-8:] because this
            # is always the same for all file paths.
    file_date = pd.Timestamp(os.path.split(filename)[0][-8:])
    x = []
    with open(filename, 'rb') as f:
        lines = [line.decode('unicode_escape') for line in f.readlines()]
        #lines = f.readlines()
        lines = [[token.replace(',', '.').strip() for token in line.split(';')[:8]] for line in lines]

    #lines = f.readlines()
        if len(lines) > 0:
        #if trace:
            print(f'reading {f} ...')
            #lines = [line.decode('unicode_escape') for line in lines]
            #lines = [[token.replace(',', '.').strip() for token in line.split(';')[:8]] for line in lines]
            lines = [line for line in lines if len(line) == 8]
            for line in lines[1:]:
                if len(line[0]) != 8:
                    line[0] = 'NaT'
                for i in range(1, len(line)):
                    try:
                        line[i] = float(line[i])
                    except ValueError:
                        line[i] = np.nan
            data = pd.DataFrame(lines[1:], columns=lines[0]).set_index('UTC time')  # .astype(float)
            data.index = pd.to_datetime(data.index)
            start_time = data.index[0]
            data_today = data[data.index >= start_time]
            data_today.index = data_today.index - data_today.index[0].floor('D') + file_date
            x.append(data_today)
            data_tomorrow = data[data.index < start_time]
            if len(data_tomorrow) > 0:
                file_date += pd.Timedelta('1D')
                data_tomorrow.index = data_tomorrow.index - data_tomorrow.index[0].floor('D') + file_date
                x.append(data_tomorrow)
        else:
            #if trace:
                print(f'no data in {f}')

    x = pd.concat(x).sort_index().resample(res).mean()

    return x
